PostgreSQL Anonymizer Development Team
===============================================================================

This is an open project. Feel free to join us and improve this tool. To find out
how you can get involved, please read [CONTRIBUTING.md].


[CONTRIBUTING.md]: CONTRIBUTING.md

Maintainer
-------------------------------------------------------------------------------

* Damien Clochard (@daamien)


Contributors
-------------------------------------------------------------------------------

* Travis Miller (@travismiller) : MacOS support
* Jan Birk (@jabi27) : Install on Ubuntu
* Olleg Samoylov (@Splarv) : Issue #87
* Damien Cazeils (www.damiencazeils.com) : Logo
* Ioseph Kim (@i0seph) : Documentation 
* Matiss Zarkevics (@leovingi) : Tests on Amazon RDS
* Peter Goodwin (@Postgressor) : Tests
* Tim (@roconda) : Documentation
* Michał Lipka (@michallipka) : Tests and typos
* Thibaut Madeleine (@madtibo) : original idea :-)
